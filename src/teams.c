#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "teams.h"

Country *teamList[NUMBER_OF_TEAMS];
static int _teamLastIndex = 0;

//Construtor para struct de nova equipe:
static Country *NewTeam(char *name){
    teamList[_teamLastIndex] = (Country*)malloc(sizeof(Country));
    teamList[_teamLastIndex]->name = name; 
    for (int i=1; i<=11; i++){
        teamList[_teamLastIndex]->team = (int*)malloc(sizeof(int)*11);
        teamList[_teamLastIndex]->team[i] = i+(_teamLastIndex-1)*11;
    }    
    _teamLastIndex += 1;
    return teamList[_teamLastIndex-1];
}

//Cria os times da copa:
void GenerateTeams(){
    NewTeam("Russia");
    NewTeam("Australia");
    NewTeam("Belgica");
    NewTeam("Brasil");
    NewTeam("Colombia");
    NewTeam("Costa Rica");
    NewTeam("Croacia");
    NewTeam("Dinamarca");
    NewTeam("Egito");
    NewTeam("Inglaterra");
    NewTeam("Franca");
    NewTeam("Alemanha");
    NewTeam("Islândia");
    NewTeam("Ira");
    NewTeam("Japao");
    NewTeam("Coreia do Sul");
    NewTeam("Marrocos");
    NewTeam("Nigeria");
    NewTeam("Panama");
    NewTeam("Peru");
    NewTeam("Polonia");
    NewTeam("Portugal");
    NewTeam("Arabia Saudita");
    NewTeam("Senegal");
    NewTeam("Servia");
    NewTeam("Espanha");
    NewTeam("Suecia");
    NewTeam("Suica");
    NewTeam("Tunisia");
    NewTeam("Uruguai");
}